package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	
	private static Connection getConnection() throws SQLException, IOException {
		Properties prop = new Properties();
		FileInputStream file = new FileInputStream("config.properties");
		prop.load(file);
		String URL = prop.getProperty("URL");
		String USER = prop.getProperty("USER");
		String PASS = prop.getProperty("PASS"); 
		return DriverManager.getConnection(URL, USER, PASS);
	}

	public static void main(String[] args) {
		Connection con;
		String query = "SELECT * FROM CLIENTES";
		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);
			while(rs.next()) System.out.printf("DNI: %s, Nombre: %s, Apellidos: %s\n", 
					rs.getString("DNI"), rs.getString("Nombre"), rs.getString("Apellidos"));
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

}
